import express from "express";
import { AQIController } from "../controllers/aqiController";
import { LocationController } from "../controllers/locationController";

const router = express.Router();
const aqiController = new AQIController();
const locationController = new LocationController();

router.get("/aqi", async (_req, res) => {
  const response = await aqiController.getGlobalData();
  return res.json(response);
});

router.get("/aqi/:source/:lat/:long", async (req, res) => {
  const { source, lat, long } = req.params;
  const response = await aqiController.getAQIData(source, lat, long);
  return res.json(response);
});

router.get("/locations/:text", async (req, res) => {
  const { text } = req.params;
  const response = await locationController.searchLocations(text);
  return res.json(response);
});

export default router;
