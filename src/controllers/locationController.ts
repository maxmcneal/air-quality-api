import { Get, Route } from "tsoa";
import fetch from "node-fetch";

// Interface for location data
interface LocationResp {
  results?: Location[];
  query?: {
    text?: string;
    parsed?: {
      city?: string;
      expected_type?: string;
    };
  };
}

interface Location {
  country: string;
  country_code: string;
  state: string;
  city: string;
  datasource: {
    sourcename: string;
    attribution: string;
    license: string;
    url: string;
  };
  lon: number;
  lat: number;
  population: number;
  result_type: string;
  formatted: string;
  address_line1: string;
  address_line2: string;
  category: string;
  timezone: {
    name: string;
    offset_STD: string;
    offset_STD_seconds: number;
    offset_DST: string;
    offset_DST_seconds: number;
  };
  plus_code: string;
  plus_code_short: string;
  rank: {
    confidence: number;
    confidence_city_level: number;
    match_type: string;
  };
  place_id: string;
  bbox: {
    lon1: number;
    lat1: number;
    lon2: number;
    lat2: number;
  };
}

@Route("/locations")
export class LocationController {
  /**
   * Search for locations based on the provided text.
   * @param text - The text to search for locations.
   * @returns A promise resolving to the location data.
   */
  @Get("/:text")
  public async searchLocations(text: string): Promise<LocationResp> {
    try {
      const data = await fetch(
        `https://api.geoapify.com/v1/geocode/autocomplete?text=${text}&format=json&apiKey=${process.env.GEOAPIFY}`,
        { method: 'GET' }
      ).then(response => response.json() as any as LocationResp);
      return data;
    } catch (err) {
      // Handle errors gracefully
      console.error("Error occurred while fetching locations:", err);
      return {};
    }
  }
}
