interface GlobalDataResp {
  g: number[]; // Latitude and longitude pair
  n: string;           // Name of the location
  u: string;           // Timestamp in string format
  a: string;           // Air quality index
  t: string;           // Timezone offset
  x: string;           // Identifier
}

import { Get, Route } from "tsoa";
import fetch from 'node-fetch';

@Route("/aqi")
export class AQIController {
  /**
   * Fetches global air quality index data.
   */
  @Get("/")
  public async getGlobalData(): Promise<GlobalDataResp[]> {
    return await this.fetchGlobalData();
  }

  /**
   * Fetches air quality index data for specific coordinates and source.
   * @param source - The data source (iqair, openweathermap, waqi).
   * @param lat - Latitude coordinate.
   * @param long - Longitude coordinate.
   */
  @Get("/:source/:lat/:long")
  public async getAQIData(source: string, lat: string, long: string): Promise<GlobalDataResp> {
    // Validate coordinates
    if (!this.isValidCoordinates(lat, long)) {
      throw new Error("Bad Request: invalid coordinates");
    }

    // Fetch data based on the specified source
    switch (source) {
      case "iqair": {
        return this.getIQAirData(lat, long);
      }
      case "openweathermap": {
        return this.getOpenWeatherMapData(lat, long);
      }
      case "waqi": {
        return this.getWAQIData(lat, long);
      }
      default: {
        throw new Error(`Bad Request: "${source}" is not one of "iqair", "openweathermap", "waqi"`);
      }
    }
  }

  /**
   * Fetches global air quality index data.
   */
  private async fetchGlobalData(): Promise<GlobalDataResp[]> {
    const request = await fetch(`https://waqi.info/rtdata/?_=${Date.now()}`);
    const file = await request.json() as { path?: string }; 

    if (file && file.hasOwnProperty('path')) { 
      const level1Data = await fetch(`https://waqi.info/rtdata/${file.path}/level1.json`)
        .then((res) => res.json() as any as GlobalDataResp);

      const stationsData = await fetch(`https://waqi.info/rtdata/${file.path}/000.json`)
        .then((res) => res.json() as any as GlobalDataResp);

      return [level1Data, stationsData];
    }
    return [];
  }

  /**
   * Fetches air quality index data from IQAir API.
   * @param lat - Latitude coordinate.
   * @param long - Longitude coordinate.
   */
  private async getIQAirData(lat: string, long: string): Promise<any> {
    const response = await fetch(
      `http://api.airvisual.com/v2/nearest_city?lat=${lat}&lon=${long}&key=${process.env.IQ_AIR_API}`
    );
    return response.json() as any; // Define expected structure
  }

  /**
   * Fetches air quality index data from OpenWeatherMap API.
   * @param lat - Latitude coordinate.
   * @param long - Longitude coordinate.
   */
  private async getOpenWeatherMapData(lat: string, long: string): Promise<any> {
    const TIME_AGO = (Date.now() - 1000 * 60 * 60 * 24) / 1000;

    const [airPollutionData, geoData] = await Promise.all([
      fetch(
        `http://api.openweathermap.org/data/2.5/air_pollution/history?lat=${lat}&lon=${long}&start=${Math.floor(
          TIME_AGO
        )}&end=${Math.floor(Date.now() / 1000)}&appid=${process.env.OPEN_WEATHER_API
        }`
      ).then((res) => res.json() as any), // Define expected structure
      fetch(
        `http://api.openweathermap.org/geo/1.0/reverse?lat=${lat}&lon=${long}&limit=1&appid=${process.env.OPEN_WEATHER_API}`
      ).then((res) => res.json() ),
    ]);

    return { airPollutionData, geoData };
  }

  /**
   * Fetches air quality index data from WAQI API.
   * @param lat - Latitude coordinate.
   * @param long - Longitude coordinate.
   */
  private async getWAQIData(lat: string, long: string): Promise<any> {
    const response = await fetch(
      `https://api.waqi.info/feed/geo:${lat};${long}/?token=${process.env.WAQI_API}`
    );
    return response.json() as any; // Define expected structure
  }

  /**
   * Validates coordinates.
   * @param lat - Latitude coordinate.
   * @param long - Longitude coordinate.
   */
  private isValidCoordinates(lat: string, long: string) {
    return (
      isFinite(+lat) && Math.abs(+lat) <= 90 &&
      isFinite(+long) && Math.abs(+long) <= 180
    );
  }
}
